-i https://pypi.org/simple

aiohttp==3.8.1; python_version >= '3.6'

aiosignal==1.2.0; python_version >= '3.6'

async-timeout==4.0.2; python_version >= '3.6'

asyncio==3.4.3

attrs==22.1.0; python_version >= '3.5'

certifi==2022.6.15; python_version >= '3.6'

chardet==3.0.4

charset-normalizer==2.1.1; python_version >= '3.6'

discord.py==2.0.1; python_full_version >= '3.8.0'

discord==2.0.0

frozenlist==1.3.1; python_version >= '3.7'

googletrans==3.0.0

h11==0.9.0

h2==3.2.0

hpack==3.0.0

hstspreload==2022.9.1; python_version >= '3.6'

httpcore==0.9.1; python_version >= '3.6'

httpx==0.13.3; python_version >= '3.6'

hyperframe==5.2.0

idna==2.10; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'

multidict==6.0.2; python_version >= '3.7'

python-dateutil==2.8.2

python-dotenv==0.20.0

requests==2.28.1

rfc3986==1.5.0

six==1.16.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'

sniffio==1.3.0; python_version >= '3.7'

toml==0.10.2

urllib3==1.26.12; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5' and python_version < '4'

yarl==1.8.1; python_version >= '3.7'

