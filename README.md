# Botbot

Custom discord bot developed for a specific discord server and to make his developers increase their discord bot programming skills in a playful way !

<https://discordpy.readthedocs.io/en/latest/ext/commands/api.html>
<https://discordpy.readthedocs.io/en/latest/api.html>

## Dev setup

To make it work from [discord developer portal](https://discord.com/developers/applications) :

- use OAuth2 link with *Send Messages*, *Read Message History* and *Add Reactions* permissions as with this link from OAuth2 menu ***discord.com/api/oauth2/authorize?client_id=\<bot_id\>&permissions=67648&scope=bot***
- enable **Server Members Intent** from the bot menu

You need to have python 3.10 installed.

```bash
python3 -m pip install pipenv
git clone https://github.com/Boufcoulman/wholesome-translator.git
cd wholesome-translator
pipenv shell
pipenv install
```

Then you need to :

- create the "config.toml" file from "config_default.toml" file with the discord token associated with your bot and the adequate file paths.
- create the ".env" file with the path to the "config.toml" file in this folder. `CONFIG_PATH=config.toml` should work.

You can then run the bot with `python ./wholesome_translator_bot.py`.

## Docker debug

Once the bot is working out of docker, you can use the few commands to run it inside docker :

```sh
docker compose rm -f
dos2unix wholesome.cron # If editing cron file with a windows device
docker compose build
docker compose up
```

The few commands help debugging :

```sh
docker ps
docker exec -ti container_name sh
```

## Prod setup

You just need to follow previous steps concerning config.toml generation and `docker compose up` command.
