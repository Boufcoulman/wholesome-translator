FROM python:3.12

RUN apt update && apt upgrade -y
RUN apt install -y cron

RUN pip install -U pip
RUN pip install pipenv

WORKDIR /opt/

# Copy pipenv files
COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock

RUN pipenv install --system

# Add cron file
COPY ./wholesome.cron /etc/cron.d/wholesome.cron
RUN chmod 0644 /etc/cron.d/wholesome.cron && crontab /etc/cron.d/wholesome.cron

# Add python scripts
COPY ./.env_default ./.env
COPY ./ext ./ext
COPY ./lib ./lib

COPY ./wholesome_translator_bot.py ./wholesome_translator_bot.py

COPY ./announce_bot.py ./announce_bot.py


ENTRYPOINT ["python"]
